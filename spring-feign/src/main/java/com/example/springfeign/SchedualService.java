package com.example.springfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "eureka-client-1",fallback = SchedualServiceImpl.class)  //指定是 rureka 客户端名称
public interface SchedualService {


    @RequestMapping(value={"/hi"})
    String hi(@RequestParam String name);

    //java 1.8之后的 default方法
//    public default void aa(){
//        System.out.println("222");
//    }

}
