package com.example.springfeign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FeignController {

    @Autowired
    SchedualService schedualService;

    @RequestMapping(value={"/hi"})
    @ResponseBody
    public String hi(@RequestParam String name){
        return schedualService.hi(name);
    }
}
