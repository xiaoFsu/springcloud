package com.example.springribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrix   //断路器  容错，当发生错误时转发到其他方法上  防止线程堵塞
public class SpringRibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRibbonApplication.class, args);
    }

    /**
     *  作者：苏晓峰
     *  时间：2019年3月20日14:05:47
     *  在工程的启动类中,通过@EnableDiscoveryClient向服务中心注册；
     *  并且向程序的ioc注入一个bean: restTemplate;
     *  并通过@LoadBalanced注解表明这个restRemplate开启负载均衡的功能。
     */
    @Bean
    @LoadBalanced  //注解表明这个restRemplate开启负载均衡的功能。
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
