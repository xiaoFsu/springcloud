package com.example.springribbon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RibbonController {

    @Autowired
    RibbonService ribbonService;

    @RequestMapping(value={"/hi"})
    @ResponseBody
    public String hi(@RequestParam String name){
        return ribbonService.hiService(name);
    }
}
