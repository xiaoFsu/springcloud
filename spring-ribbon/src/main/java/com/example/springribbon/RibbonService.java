package com.example.springribbon;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RibbonService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "Error")
    public String hiService(String name){
        return restTemplate.getForObject("http://EUREKA-CLIENT-1/hi?name="+name,String.class);
    }

    public String Error(String name){
        return "抱歉，当前服务不可访问";
    }

}
