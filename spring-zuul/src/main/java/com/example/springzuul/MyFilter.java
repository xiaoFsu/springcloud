package com.example.springzuul;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 *  作者：苏晓峰
 *  时间：2019年3月20日15:12:57
 *  自定义过滤器来实现zuul过滤器
 *
 */

@Component
public class MyFilter extends ZuulFilter {


    /**
     *  作者：苏晓峰
     *  时间：2019年3月20日15:32:59
     *
     *
     *  filterType：返回一个字符串代表过滤器的类型，在zuul中定义了四种不同生命周期的过滤器类型，具体如下：
             * pre：路由之前
             * routing：路由之时
             * post： 路由之后
             * error：发送错误调用
     *
     *
     * filterOrder：过滤的顺序
     *
     * shouldFilter：这里可以写逻辑判断，是否要过滤，本文true,永远过滤。
     *
     * run：过滤器的具体逻辑。可用很复杂，包括查sql，nosql去判断该请求到底有没有权限访问。
     *
     */

    //使用自带的日志输出来进行输出必要信息
    private static Logger logger = LoggerFactory.getLogger(MyFilter.class);

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;  //选择过滤
    }

    @Override
    public Object run() throws ZuulException {
        logger.info("进行过滤...");
        //请求中获取token值，如果有则正常 没有则输出token是空的

        //获取当前请求内容
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = context.getRequest();

        //获取请求路径中的参数
        String token = request.getParameter("token");
        if(token == null){
            //无token
            logger.error("token is empty！");
            //设置返回翻出
            context.setSendZuulResponse(false);
            context.setResponseStatusCode(401);
            //返回token is empty
            try{
                context.getResponse().getWriter().write("token is empty");
            }catch (Exception e){
                logger.error(e.toString());
            }
            return null;
        }

        logger.info("ok,Token is :"+token);
        return null;
    }
}
